
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:43PM

See merge request itentialopensource/adapters/adapter-salt!20

---

## 0.4.3 [09-16-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-salt!18

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_19:00PM

See merge request itentialopensource/adapters/adapter-salt!17

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_20:12PM

See merge request itentialopensource/adapters/adapter-salt!16

---

## 0.4.0 [05-09-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/devops-netops/adapter-salt!15

---

## 0.3.4 [03-26-2024]

* Changes made at 2024.03.26_14:24PM

See merge request itentialopensource/adapters/devops-netops/adapter-salt!14

---

## 0.3.3 [03-11-2024]

* Changes made at 2024.03.11_13:22PM

See merge request itentialopensource/adapters/devops-netops/adapter-salt!13

---

## 0.3.2 [02-26-2024]

* Changes made at 2024.02.26_13:47PM

See merge request itentialopensource/adapters/devops-netops/adapter-salt!12

---

## 0.3.1 [12-29-2023]

* remove docker

See merge request itentialopensource/adapters/devops-netops/adapter-salt!11

---

## 0.3.0 [12-29-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/devops-netops/adapter-salt!10

---

## 0.2.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/devops-netops/adapter-salt!3

---

## 0.1.3 [03-14-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/devops-netops/adapter-salt!2

---

## 0.1.2 [02-05-2021]

- Fix issues with the last migration

See merge request itentialopensource/adapters/devops-netops/adapter-salt!1

---

## 0.1.1 [01-18-2021]

- Initial Commit

See commit 33b24d4

---
